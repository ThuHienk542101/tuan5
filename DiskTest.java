package hust.soict.hespi.aims;
import hust.soict.hespi.aims.order.*;
import hust.soict.hespi.aims.disc.*;

public class DiskTest {
	public static void main(String[] args) {
		Order anOrder = new Order();
		DigitalVideoDisc[] dvdList = new DigitalVideoDisc[10];
		dvdList[0] = new DigitalVideoDisc("The Lion King", "Animation", "Roger Allers", 87, 19.95f);
		dvdList[1] = new DigitalVideoDisc("Star Wars", "Science Fiction", "George Lucas", 124, 24.95f);
		dvdList[2] = new DigitalVideoDisc("Aladdin", "Animation", "John Musker", 90, 18.99f);
		dvdList[3] = new DigitalVideoDisc("Star Wars 2", "Science Fiction", "George Lucas", 124, 24.95f);
		anOrder.addDigitalVideoDisc(dvdList[0], dvdList[1]);
		anOrder.addDigitalVideoDisc(dvdList[2]);
		System.out.println(dvdList[0].search("Lion King"));
		System.out.println(anOrder.getALuckyItem().getTitle());
		anOrder.printOrdered();
	}
}
