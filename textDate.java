package hust.soict.hespi.test.utils;
import hust.soict.hespi.aims.utils.DateUtils;
import hust.soict.hespi.aims.utils.MyDate;

public class textDate{
    public static void main(String[] args) {
    	MyDate date = new MyDate("first","February","twenty nineteen");
    	System.out.println("Day: "+date.getDay()+" Month: "+date.getMonth()+" Year: "+date.getYear());  
//    	date.print();
//    	date.printFormat();
    	
    	MyDate dateArr[] = new MyDate[4];
    	dateArr[0]=new MyDate(12,2,2020);
    	dateArr[1]=new MyDate(18,1,1999);
    	dateArr[2]=new MyDate(28,8,2005);
    	dateArr[3]=new MyDate(12,2,2020);
    	
    	int kqua =DateUtils.compareMyDate(dateArr[0], dateArr[1]);
    	if(kqua==1) {
    		System.out.println("ngay 1 > ngay 2");
    	}else if(kqua==-1) {
    		System.out.println("ngay 1 < ngay 2");
    	}else if(kqua==0) {
    		System.out.println("ngay 1 = ngay 2");
    	}
    	DateUtils.sortMyDate(dateArr);
    	DateUtils.print(dateArr);
    }
}