package hust.soict.hespi.aims.disc;


public class DigitalVideoDisc {
	
	private String title;
	private String category;
	private String director;
	private int length;
	private float cost;
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getCategory() {
		return category;
	}
	
	public void setCategory(String category) {
		this.category = category;
	}
	
	public String getDirector() {
		return director;
	}
	
	public void setDirector(String director) {
		this.director = director;
	}
	
	public int getLength() {
		return length;
	}
	
	public boolean setLength(int length) {
		if(length > 0) { 
			this.length = length;
			return true;
		}else 
			return false;
	}
	
	public float getCost() {
		return cost;
	}
	
	public boolean setCost(float cost) {
		if(cost > 0) {
			this.cost = cost;
			return true;
		}else 
			return false;
	}
	
	public DigitalVideoDisc() {
		this.title = "noname";
		this.category = "unknown";
		this.director = "unknown";
		this.length = 0;
		this.cost = 0.0f;
	}
	
	public DigitalVideoDisc(String title) {
		this.title = title;
	}
	
	public DigitalVideoDisc(String title, String category) {
		this.title = title;
		this.category = category;
	}
	
	public DigitalVideoDisc(String title, String category, String director) {
		this.title = title;
		this.category = category;
		this.director = director;
	}
	
	public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
		this.title = title;
		this.category = category;
		this.director = director;
		this.length = length;
		this.cost = cost;
	}
	
	public boolean search(String title) {
		String[] parts= title.split(" ");
		String[] parts_title= this.title.split(" ");
	    for(int i=0; i<parts.length; i++) {
//	    	System.out.println(parts[i]);
	    	boolean test=false;
	    	for(int j=0; j<parts_title.length; j++) {
//	    		System.out.println(parts[i]);
	    		 if(parts[i].equals(parts_title[j])) {
	    			 test = true;
	    			 break;
	    		 }
	    	}
	    	if(!test) {
	    		return false;
	    	}
	    }
		return true;
	}
}
